select distinct A.model as lap_mod1, B.model as lap_mod2
from Laptop A, Laptop B
where A.ram = B.ram and A.model != B.model;